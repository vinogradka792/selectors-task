var findBtn = document.querySelector(".selector-find");
var nextBtn = document.getElementById("btn-next");
var prevBtn = document.getElementById("btn-prev");
var nextSiblingBtn = document.querySelector(".nav-right");
var prevSiblingBtn = document.querySelector(".nav-left");
var parentBtn = document.querySelector(".nav-top");
var childBtn = document.querySelector(".nav-bottom");
var grandfatherBtn = document.querySelector(".nav-grandfather");
var grandsonBtn = document.querySelector(".nav-grandson");

var input = document.querySelector(".selector");

var selector = '';
var collection = [];
var activeCollectionIndex = 0;
var activeElement = null;

function highlightElement(elem){
    elem.setAttribute('style', 'outline: 4px solid red; background:skyblue')
}

function unHighlightElement(elem){
    elem.setAttribute('style', '')
}

findBtn.addEventListener('click', () =>{
    var nevWal = input.value;

    var highlightedElement = activeElement || collection[activeCollectionIndex];

    if(highlightedElement){
        unHighlightElement(highlightedElement);
        activeElement = null;
        activeCollectionIndex = 0;
    }

    if(nevWal){
        selector = input.value;
        collection = document.querySelectorAll(selector);
        toggleButtons(checkButtonsAvailability());
        highlightElement(collection[0]);
    }
    else{
        disableButtons();
    }
})

nextBtn.addEventListener('click', () =>{
    activeCollectionIndex += 1;
    unHighlightElement(collection[activeCollectionIndex - 1]);
    highlightElement(collection[activeCollectionIndex]);

    toggleButtons(checkButtonsAvailability())
})

prevBtn.addEventListener('click', () =>{
    activeCollectionIndex -= 1;
    unHighlightElement(collection[activeCollectionIndex + 1]);
    highlightElement(collection[activeCollectionIndex]);

    toggleButtons(checkButtonsAvailability())
})

nextSiblingBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.nextElementSibling;

    bottomPanelNavigation(currentElem, nextElement);
})

prevSiblingBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.previousElementSibling;

    bottomPanelNavigation(currentElem, nextElement);
})

childBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.children[0];

    bottomPanelNavigation(currentElem, nextElement);
})

parentBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.parentElement;

    bottomPanelNavigation(currentElem, nextElement);
})

grandfatherBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.parentElement.parentElement;

    bottomPanelNavigation(currentElem, nextElement);
})

grandsonBtn.addEventListener('click', () =>{
    var currentElem = activeElement || collection[activeCollectionIndex];
    var nextElement = currentElem.children[0].children[0];

    bottomPanelNavigation(currentElem, nextElement);
})

function toggleButtons(config){
    grandfatherBtn.disabled = config.disableGrandFatherButton || false;
    grandsonBtn.disabled = config.disableNextGrandsonButton || false;

    nextBtn.disabled = config.disableNextButton || false;
    prevBtn.disabled = config.disablePrevButton || false;
    nextSiblingBtn.disabled = config.disableNextSiblingButton || false;
    prevSiblingBtn.disabled = config.disablePrevSiblingButton || false;
    parentBtn.disabled = config.disableParentButton || false;
    childBtn.disabled = config.disableChildButton || false;
}

function disableTopNavigationButtons(){
    nextBtn.disabled = true;
    prevBtn.disabled = true;
}

function disableButtons(){
    toggleButtons({
        disableGrandFatherButton: true,
        disableNextGrandsonButton: true,

        disableNextButton: true,
        disablePrevButton: true,
        disablePrevSiblingButton: true,
        disableNextSiblingButton: true,
        disableParentButton: true,
        disableChildButton: true,
    })
}

function checkButtonsAvailability(){
    var currentElem = activeElement  ||  collection[activeCollectionIndex];

    return{
        disableGrandFatherButton: !(currentElem && currentElem.parentElement && currentElem.parentElement.parentElement),
        disableNextGrandsonButton: !(currentElem && currentElem.children[0] && currentElem.children[0].children[0]),

        disableNextButton: activeCollectionIndex === collection.length - 1,
        disablePrevButton: activeCollectionIndex === 0,
        disablePrevSiblingButton: !currentElem.previousElementSibling,
        disableNextSiblingButton: !currentElem.nextElementSibling,
        disableParentButton: !currentElem.parentElement,
        disableChildButton: !currentElem.children.length,
    }
}

function bottomPanelNavigation(current, prev){
    var currentElem = current;
    var nextElement = prev;

    unHighlightElement(currentElem);
    highlightElement(nextElement);

    activeElement = nextElement;

    toggleButtons(checkButtonsAvailability())
    disableTopNavigationButtons();
}
